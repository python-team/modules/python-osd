python-osd (0.2.14-7) UNRELEASED; urgency=medium

  [ Ondřej Nový ]
  * Fixed VCS URL (https)
  * d/control: Set Vcs-* to salsa.debian.org
  * d/changelog: Remove trailing whitespaces
  * d/control: Remove trailing whitespaces
  * Convert git repository from git-dpm to gbp layout
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Sandro Tosi ]
  * Use the new Debian Python Team contact name and address

 -- Sandro Tosi <morph@debian.org>  Mon, 04 Jan 2021 17:06:20 -0500

python-osd (0.2.14-6) unstable; urgency=medium

  [ Jakub Wilk ]
  * Use canonical URIs for Vcs-* fields.

  [ Javi Merino ]
  * Team upload
  * Acknowledge NMU
  * Convert to dh_python2
  * Add misc:Depends to python-pyosd

 -- Javi Merino <vicho@debian.org>  Thu, 28 Aug 2014 20:22:35 -0700

python-osd (0.2.14-5.1) unstable; urgency=low

  * Non-maintainer upload.
  * Fix "binary-indep doesn't build python-osd":
    update binary-indep and binary-arch targets in debian/rules.
    (Closes: #650788)

 -- gregor herrmann <gregoa@debian.org>  Tue, 24 Jan 2012 19:02:00 +0100

python-osd (0.2.14-5) unstable; urgency=low

  * Rename of the binary package from "python-osd" to "python-pyosd"
    which is the actual name of the Python module.
  * Changed the maintainer's e-mail address.
  * Standards-Version bumped to 3.8.1
  * debhelper compat bumped to 7
  * debian/control
    - switch Vcs-Browser field to viewsvn
    (Thanks to Sandro Tosi)

 -- Mauro Lizaur <mauro@cacavoladora.org>  Thu, 04 Jun 2009 16:42:12 -0300

python-osd (0.2.14-4) unstable; urgency=medium

  * Fixed missing dependencies on libxosd2 using
    ${shlibs:Depends} to solve this. (closes: #497768)
  * Removed unnecessary dependency on python-simplejson
  * Added debian/watch file
  * $PWD replaced by $(CURDIR) to avoid a possible FTBFS on debian/rules
  * Replaced 'python' to 'Python' on the small desc of debian/control
  * Updated Standard-Version to 3.8.0

 -- Mauro Lizaur <lavaramano@gmail.com>  Tue, 23 Sep 2008 03:47:12 -0300

python-osd (0.2.14-3) unstable; urgency=low

  * Added to DPMT.

 -- Mauro Lizaur <lavaramano@gmail.com>  Wed, 27 Feb 2008 00:03:40 -0300

python-osd (0.2.14-2) unstable; urgency=high

  * Fixed arch dependent binary (closes: #461601)

 -- Mauro Lizaur <lavaramano@gmail.com>  Sun, 20 Jan 2008 17:57:16 -0300

python-osd (0.2.14-1) unstable; urgency=low

  * New Upstream version 0.2.14
  * New maintainer (closes: #440782)
  * python-twisted is now suggested (closes: #431172)
  * Updated python-support in debian/control
  * Standards-Version updated to 3.7.3
  * URL changed in debian/copyright
  * added debian/compat and removed the variable DH_COMPAT from
    debian/rules
  * Upstream now supports non-western characters in cases where
    it wouldn't work before, bugfixes battery module and a cleaner
    fail is provided in case the font is unavailable.

 -- Mauro Lizaur <lavaramano@gmail.com>  Thu, 10 Jan 2008 22:49:14 -0300

python-osd (0.2.12-1.2) unstable; urgency=low

  * Non-maintainer upload.
  * Move dh_installdeb *after* dh_pysupport (Closes: #382769),
    thanks to Jeremie Corbier.

 -- Pierre Habouzit <madcoder@debian.org>  Sun, 13 Aug 2006 13:05:25 +0200

python-osd (0.2.12-1.1) unstable; urgency=low

  * Non-maintainer upload.
  * Update package to the new python policy (Closes: #373332).
  * Add missing depends on python-twisted (Closes: #320167).

 -- Pierre Habouzit <madcoder@debian.org>  Fri, 30 Jun 2006 01:45:42 +0200

python-osd (0.2.12-1) unstable; urgency=low

  * New upstream release. Closes: #290724.
  * Thanks for the NMU. Closes: #240549, #248774.
  * Bump the standards version to 3.6.1.
  * Dropping python 2.1 and 2.2, adding 2.4. Closes: #266435.
  * Set section to python to make linda happy.

 -- Tommi Virtanen <tv@debian.org>  Fri, 28 Jan 2005 02:06:39 +0200

python-osd (0.2.6-1.1) unstable; urgency=low

  * NMU.
  * debian/control: We can't recommend twisted for the 2.1 package, as twisted
    upstream stopped supporting Python 2.1 (which led to the removal of the
    Debian package) (Closes: #240549)
  * debian/rules: Remove setgid bits from the build dir. Report and Patch
    from Matt Kraai, thanks! (#248774)

 -- Marc 'HE' Brockschmidt <he@debian.org>  Fri,  9 Jul 2004 10:27:02 +0200

python-osd (0.2.6-1) unstable; urgency=low

  * Use python2.3 by default.
  * Fix typo in python2.1-osd description.
  * Thanks for the NMU.

 -- Tommi Virtanen <tv@debian.org>  Sat,  9 Aug 2003 15:04:39 +0300

python-osd (0.2.6-0.1) unstable; urgency=low

  * NMU.
  * New upstream version.
  * Fixes build error (closes: #191522).
  * Tighten build dependency on libxosd-dev.

 -- Matthias Klose <doko@debian.org>  Sat, 10 May 2003 14:04:09 +0200

python-osd (0.2.5-1) unstable; urgency=low

  * New upstream release. Includes rewritten daemon.py (Closes: #154810).
  * Build-depend on libxosd-dev >=1.0.4-1.
  * Use python2.2 by default.
  * Support python2.3 (remember to add back the Recommends on
    python2.3-twisted as soon as that is available).
  * Bump policy to 3.5.7.

 -- Tommi Virtanen <tv@debian.org>  Sun, 22 Sep 2002 14:54:15 +0300

python-osd (0.2.4-1) unstable; urgency=low

  * Initial Release.
  * Temporarily disabled xosd_set_max_lines() and xosd_error
    usage in _pyosd.c, upstream xosd doesn't support them yet.

 -- Tommi Virtanen <tv@debian.org>  Sun, 21 Jul 2002 18:16:50 +0300
